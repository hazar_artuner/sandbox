<?php namespace App\Instagram\Service;

use App\Instagram\Model\Instagram;
use Common\Model\Interfaces\ICache;

class InstagramService{

    /** @var ICache */
    private $CacheDriver;

    /** @var Instagram */
    private $Instagram;

    private $mainInstagramUserId = "3147523808";  // Account: hazar.artuner

    private $mainUserMediaLimit = 10;

    public function __construct(Instagram $Instagram, ICache $CacheDriver){
        $this->Instagram = $Instagram;
        $this->CacheDriver = $CacheDriver;
    }

    /**
     * This method collects the MainUser's media list which are liked by the given user
     *
     * @param $userId
     * @return array
     */
    public function matchUserLikedMedia($userId){
        $matchedMedias = [];

        // Get the media list to check
        $mediaList = $this->getMainInstagramUserMediaList($this->mainUserMediaLimit);

        foreach($mediaList as $media){
            if(sizeof($media->liked_users) > 0){
                foreach ($media->liked_users as $user) {
                    if($user->id == $userId){
                        $matchedMedias[] = $media;
                    }
                }
            }
        }

        return $matchedMedias;
    }


    /**
     * This method collects the MainUser's media list which are liked
     * by the given user's friends.
     *
     * @param $userId
     * @param $friendLimit
     * @return array
     */
    public function matchUserFriendsLikedMedia($userId, $friendLimit){
        // Get the media list to check
        $mediaList = $this->getMainInstagramUserMediaList($this->mainUserMediaLimit);

        $follows = $this->getUserFollows($userId, $friendLimit);

        $matchedMedias = [];

        if(sizeof($follows->data) > 0 && sizeof($mediaList) > 0){

            foreach($mediaList as $media){

                if(sizeof($media->liked_users) > 0){

                    foreach($media->liked_users as $likedUser){

                        foreach($follows->data as $friend){

                            if($likedUser->id == $friend->id) {
                                $matchedMedias[] = $media;
                            }
                        }
                    }
                }
            }
        }

        return $matchedMedias;
    }

    private function getMainInstagramUserMediaList($limit){
        $cacheKey = __METHOD__ . "-" . $this->mainInstagramUserId . "-"  . $limit;

        if($this->CacheDriver){
            $result = $this->CacheDriver->get($cacheKey);
        }

        if(empty($result)){
            $result = $this->Instagram->getUserMediaListWithLikedUsers($this->mainInstagramUserId, $limit);

            if($this->CacheDriver){
                $this->CacheDriver->set($cacheKey, $result);
            }
        }

        return $result;
    }

    public function getUserFollows($userId, $limit){
        $cacheKey = __METHOD__ . "-" . $userId . "-"  . $limit;

        if($this->CacheDriver){
            $result = $this->CacheDriver->get($cacheKey);
        }

        if(empty($result)){
            $result = $this->Instagram->getUserFollows($userId, $limit);

            if($this->CacheDriver){
                $this->CacheDriver->set($cacheKey, $result);
            }
        }

        return $result;
    }


}