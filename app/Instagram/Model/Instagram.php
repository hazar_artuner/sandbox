<?php
/**
 * Created by PhpStorm.
 * User: hazar
 * Date: 14/04/16
 * Time: 14:40
 */

namespace App\Instagram\Model;

class Instagram extends \MetzWeb\Instagram\Instagram
{
    private $_scopes = array('basic', 'likes', 'comments', 'relationships', 'public_content', 'follower_list');


    public function getLoginUrl($scope = array('basic')) {
        if (is_array($scope) && count(array_intersect($scope, $this->_scopes)) === count($scope)) {
            return self::API_OAUTH_URL . '?client_id=' . $this->getApiKey() . '&redirect_uri=' . urlencode($this->getApiCallback()) . '&scope=' . implode('+', $scope) . '&response_type=code';
        } else {
            throw new \Exception("Error: getLoginUrl() - The parameter isn't an array or invalid scope permissions used.");
        }
    }

    /**
     * @param int $userId media's owner id
     * @param int $limit media count that will be collected from the api
     * @return array collected media list with each item has a property named "liked_users".
     * That property gives the list of users who liked the related media.
     */
    public function getUserMediaListWithLikedUsers($userId, $limit = 1000){
        $result = [];

        if($response = $this->getUserMedia($userId, $limit)){

            $result = $response->data;

            foreach($result as &$media){
                $likes = $this->getMediaLikes($media->id);
                $media->liked_users = $likes->data;
            }
        }

        return $result;
    }
}