<?php namespace App\Instagram\Controller;


use App\Instagram\Model\Instagram;
use App\Instagram\Service\InstagramService;
use Common\Model\Redis;
use Common\Service\Interfaces\ITemplate;
use Common\Service\TwigService;

class DefaultController{

    /** @var  Instagram */
    private $Instagram;

    /** @var InstagramService */
    private $InstagramService;

    /** @var ITemplate */
    private $Template;

    public function __construct(){
        $this->Instagram = new Instagram([
            "apiKey" => "3ffa643fdfa3485eabc83f03e3f91680",
            "apiSecret" => "0f1747a8a7ae43d395c541f15480e434",
            "apiCallback" => "http://sandbox.dev/Instagram/Default/callback"
        ]);

        if(isset($_SESSION["oAuthToken"])){
            $this->Instagram->setAccessToken($_SESSION["oAuthToken"]);
        }

        $this->InstagramService = new InstagramService($this->Instagram, new Redis());

        $this->Template = new TwigService(__DIR__ . "/../Views");
    }

    public function indexAction(){
        if(!$this->Instagram->getAccessToken()){
            echo $this->Template->render("login.twig");
        }
        else{
            $currentUser = $this->Instagram->getUser();

            $data['userLikedMedia'] = $this->InstagramService->matchUserLikedMedia($currentUser->data->id);
            $data['friendsLikedMedia'] = $this->InstagramService->matchUserFriendsLikedMedia($currentUser->data->id, 1000);

            echo $this->Template->render("media-list.twig", $data);
        }
    }

    public function loginAction(){
        $loginUrl = $this->Instagram->getLoginUrl([ 'basic', 'likes', 'public_content', 'relationships', 'follower_list']);

        header("Location: " . $loginUrl);
        exit;
    }

    public function logoutAction(){
        session_destroy();

        header("Location: index");
        exit;
    }

    public function callbackAction(){
        if($token = $this->Instagram->getOAuthToken($_GET["code"], true)){
            $_SESSION["oAuthToken"] = $token;

            header("Location: index");
            exit;
        }
        else{
            echo "Token alınamadı!";
        }
    }

    public function updateMainTokenAction()
    {
        $this->Instagram->setApiCallback('http://sandbox.dev/Instagram/Default/updateMainTokenCallback');

        header("Location: " . $this->Instagram->getLoginUrl([ 'basic', 'likes', 'public_content', 'relationships', 'follower_list']));
        exit;
    }

    public function updateMainTokenCallbackAction()
    {
        if ($token = $this->Instagram->getOAuthToken($_GET["code"], false)) {

            echo json_encode($token); exit;

            $_SESSION["oAuthToken"] = $token;

            $response = $this->Instagram->getUser();

            if ($response->meta->code == 200 && $response->data->id == $this->mainUserId) {
                \m_cms_setting::set('instagram-settings', 'instagram-main-token', $token);
            } else {
                echo "Please log-in to the App with <b><u>Main User</u></b>";
                exit;
            }
        } else {
            echo "Token <u><b>could not</b></u> updated!";
        }
    }

}