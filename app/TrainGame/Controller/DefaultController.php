<?php namespace App\TrainGame\Controller;
use Common\Service\TwigService;

/**
 * Created by PhpStorm.
 * User: hazar
 * Date: 08/05/16
 * Time: 16:06
 */

class DefaultController{

    /** @var  TwigService */
    private $TwigService;

    public function __construct(){
        $this->TwigService = new TwigService(__DIR__ . "/../Views/", "/app/TrainGame/Resources/public/");
    }

    public function indexAction(){

        echo $this->TwigService->render("index.twig");
    }

}