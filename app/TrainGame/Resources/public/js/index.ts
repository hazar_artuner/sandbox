export class Index{
    private name : string;
    
    private surname : string;
    
    constructor(name : string, surname : string){
        this.name = name;
        this.surname = surname;
    }
    
    public getFullname():string {
        return this.name + " " + this.surname;
    }
}