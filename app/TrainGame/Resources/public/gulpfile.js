var gulp = require("gulp");
var sass = require('gulp-sass');
var ts = require('gulp-typescript');
 
gulp.task('typescript', function () {
	return gulp.src('./js/app.ts')
		.pipe(ts({
			noImplicitAny: true,
			out: 'app.js'
		}))
		.pipe(gulp.dest('./js/'));
});
 
gulp.task('sass', function () {
  return gulp.src('./scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./scss/*.scss', ['sass']);
});

gulp.task("default", ["sass", "sass:watch", "typescript"]);
