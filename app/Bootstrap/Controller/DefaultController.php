<?php namespace App\Bootstrap\Controller;
use Common\Service\TwigService;

/**
 * Created by PhpStorm.
 * User: hazar
 * Date: 02/05/16
 * Time: 11:33
 */

class DefaultController{

    /** @var  TwigService */
    private $TwigService;

    public function __construct(){
        $this->TwigService = new TwigService( __DIR__ . "/../Views");
    }

    public function indexAction(){
        echo $this->TwigService->render("index.twig");
    }
}