<?php
/**
 * Created by PhpStorm.
 * User: hazar
 * Date: 06/07/16
 * Time: 15:32
 */

namespace App\Bootstrap\Helper;

class SampleHelper{

    private static $instance;

    private $name;
    private $surname;

    public static function getInstance(){
        if(!self::$instance){
            self::$instance = new SampleHelper();
        }

        return self::$instance;
    }

    public static function setNames($name, $surname){
        self::getInstance()->name = $name;
        self::getInstance()->surname = $surname;
    }

    public static function getNames(){
        return self::getInstance()->name . " - " . self::getInstance()->name;
    }
}