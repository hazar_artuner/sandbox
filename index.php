<?php session_start();
/**
 * Created by PhpStorm.
 * User: hazar
 * Date: 13/04/16
 * Time: 15:26
 */

require_once __DIR__ . "/vendor/autoload.php";

$app = $_GET["app"];
$controller = $_GET["controller"];
$action = $_GET["action"] ? : "index";

$controllerPath = __DIR__ . "/app/" . $app . "/Controller/" . $controller . "Controller.php";

if(!file_exists($controllerPath)){
    header("Location: /Instagram/Default/index");
    exit;
}
else{
    $class = "App\\" . $app . "\\Controller\\" . $controller . "Controller";

    $controller = new $class();

    try{
        $controller->{$action . "Action"}();
    }
    catch (Exception $e){
        var_dump($e);

        echo "Error happened: " . $e->getMessage();
    }
}
