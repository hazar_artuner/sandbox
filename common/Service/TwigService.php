<?php namespace Common\Service;

use Common\Service\Interfaces\ITemplate;

class TwigService implements ITemplate{

    private $_Twig;

    private $_loader;

    private $_resourcesPath;

    public function __construct($templatesDirectory, $resourcesPath = null)
    {
        $this->_loader = new \Twig_Loader_Filesystem($templatesDirectory);

        $this->_Twig = new \Twig_Environment($this->_loader);

        if($resourcesPath === null){
            $this->_resourcesPath = $resourcesPath;
        }
        else{
            $this->_resourcesPath = $resourcesPath;
        }
    }

    public function render($name, $data = [])
    {
        if($this->_resourcesPath){
            $data["resources"] = $this->_resourcesPath;
        }

        return $this->_Twig->render($name, $data);
    }

}