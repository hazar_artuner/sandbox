<?php namespace Common\Service\Interfaces;


interface ITemplate{

    public function __construct($templatesDirectory);

    public function render($fileName, $data = null);

}