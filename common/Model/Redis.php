<?php namespace Common\Model;

use Common\Model\Interfaces\ICache;
use Predis\Client;

class Redis implements ICache
{
    /** @var Client */
    private $redis;

    public function __construct(){
        if(!$this->redis){
            $this->redis = new Client([
                'scheme' => 'tcp',
                'host'   => '127.0.0.1',
                'port'   => 6379,
            ]);
        }
    }

    public function get($key)
    {
        $data = null;

        if($store = $this->redis->get($key)){
            $store = json_decode($store);

            switch ($store->type){
                case  "boolean":  break;

                case "integer": return (int) $store->data;  break;
                case "double": return (float) $store->data;  break;
                case "string": return (string) $store->data;  break;
                case "array": return (array) $store->data;  break;
                case "object": return (object) $store->data; break;
                case "null": return null;  break;
                default: return null;
            }
        }

        return $data;
    }

    public function set($key, $value, $timeout = 86400)
    {
        $expireTTL = $timeout === null ? : "ex";

        $store = json_encode([
            "type" => gettype($value),
            "data" => $value
        ]);

        return $this->redis->set($key, $store, $expireTTL, $timeout);
    }

    public function delete($key)
    {
        return $this->redis->del([$key]);
    }
}