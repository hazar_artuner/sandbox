<?php namespace Common\Model\Interfaces;

interface ICache{
    public function get($key);

    public function set($key, $value, $timeout = 86400);

    public function delete($key);
}