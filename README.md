# README #

### What is this repository for? ###

* This is a Sandbox repository, as you can guess through its name, this is a basic environment setup for any sandbox development process.

### How do I get set up? ###

* git clone git@bitbucket.org:hazar_artuner/sandbox.git

* go to project directory within terminal. Then execute "composer install"

### How to run? ###
* Create a new folder to the "app" folder. This will be your app name. For example it may be "MyApp". So the folder structure for "MyApp" application should look like this: "app/MyApp".

* Create a new folder named "Controller" into your newly created application folder. For this sample the result should look like that: "app/AppName/Controller/".

* Create a new Controller file into the Controller folder. For this example it may be "DefaultController.php".

* Create a new class within the newly created controller file with the same name. For this example this should be "class DefaultController".

* Create a new action method within the newly created controller class. For example "function indexAction(){ .. }". Note that each action method must end with "Action" name.

* Lastly you can access to this action by its action name like this: "http://domain-name/MyApp/Default/index". Notice that we do not use the "Action" word while trying to access it.

### Who do I talk to? ###

* Repo owner